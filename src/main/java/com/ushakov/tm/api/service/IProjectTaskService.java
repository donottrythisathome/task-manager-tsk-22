package com.ushakov.tm.api.service;

import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    // TASK CONTROLLER
    Task bindTaskByProjectId(String userId, String projectId, String taskId);

    // PROJECT CONTROLLER
    Project deleteProjectById(String userId, String projectId);

    // TASK CONTROLLER
    List<Task> findAllTasksByProjectId(String userId, String projectId);

    // TASK CONTROLLER
    Task unbindTaskFromProject(String userId, String taskId);

}
