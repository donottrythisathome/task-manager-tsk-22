package com.ushakov.tm.api.service;

import com.ushakov.tm.command.AbstractCommand;
import com.ushakov.tm.model.Command;

import java.util.List;

public interface ICommandService {

    void add(AbstractCommand command);

    AbstractCommand getCommandByArg(String arg);

    AbstractCommand getCommandByName(String name);

    List<String> getCommandNames();

    List<AbstractCommand> getCommands();

}
