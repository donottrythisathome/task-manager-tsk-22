package com.ushakov.tm.api.service;

import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.model.User;

public interface IAuthService {

    void checkRoles(Role... roles);

    User getUser();

    String getUserId();

    boolean isAuth();

    void login(String login, String password);

    void logout();

    void registry(String login, String password, String email);

}
