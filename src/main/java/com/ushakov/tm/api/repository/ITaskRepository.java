package com.ushakov.tm.api.repository;

import com.ushakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IOwnerBusinessRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

}