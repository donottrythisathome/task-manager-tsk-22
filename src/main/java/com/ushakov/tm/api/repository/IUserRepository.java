package com.ushakov.tm.api.repository;

import com.ushakov.tm.api.IRepository;
import com.ushakov.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User findOneByEmail(final String email);

    User findOneByLogin(final String login);

    User removeOneByLogin(final String login);

}
