package com.ushakov.tm.api.repository;

import com.ushakov.tm.command.AbstractCommand;
import com.ushakov.tm.model.Command;

import java.util.List;

public interface ICommandRepository {

    void add(AbstractCommand command);

    AbstractCommand getCommandByArg(String arg);

    AbstractCommand getCommandByName(String name);

    List<String> getCommandNames();

    List<AbstractCommand> getCommands();

}
