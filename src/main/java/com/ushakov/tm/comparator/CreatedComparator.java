package com.ushakov.tm.comparator;

import com.ushakov.tm.api.model.IHasCreated;

import java.util.Comparator;

public final class CreatedComparator implements Comparator<IHasCreated> {

    private static final CreatedComparator INSTANCE = new CreatedComparator();

    private CreatedComparator() {
    }

    public static CreatedComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasCreated o1, final IHasCreated o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}
