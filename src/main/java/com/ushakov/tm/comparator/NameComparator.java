package com.ushakov.tm.comparator;

import com.ushakov.tm.api.model.IHasName;

import java.util.Comparator;

public final class NameComparator implements Comparator<IHasName> {

    private static final NameComparator INSTANCE = new NameComparator();

    private NameComparator() {
    }

    public static NameComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasName o1, final IHasName o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getName().compareTo(o2.getName());
    }

}
