package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.IOwnerBusinessRepository;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.model.AbstractOwnerBusinessEntity;

import java.util.Optional;

public abstract class AbstractOwnerBusinessRepository<E extends AbstractOwnerBusinessEntity>
        extends AbstractOwnerRepository<E>
        implements IOwnerBusinessRepository<E> {

    @Override
    public E findOneByName(final String userId, final String name) {
        return findAll(userId).stream()
                .filter(e -> e.getName().equals(name))
                .findFirst()
                .orElseThrow(ObjectNotFoundException::new);
    }

    @Override
    public E removeOneByName(final String userId, final String name) {
        final E entity = findOneByName(userId, name);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

}