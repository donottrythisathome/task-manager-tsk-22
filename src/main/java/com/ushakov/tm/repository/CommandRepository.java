package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.ICommandRepository;
import com.ushakov.tm.command.AbstractCommand;
import com.ushakov.tm.model.Command;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private List<AbstractCommand> list = new ArrayList<>();

    @Override
    public void add(final AbstractCommand command) {
        list.add(command);
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        return list.stream()
                .filter(c -> c.arg().equals(arg))
                .findFirst().orElse(null);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return list.stream()
                .filter(c -> c.name().equals(name))
                .findFirst().orElse(null);
    }

    @Override
    public List<String> getCommandNames() {
        final List<String> names = new ArrayList<>();
        for (final AbstractCommand command : list) {
            names.add(command.name());
        }
        return names;
    }

    @Override
    public List<AbstractCommand> getCommands() {
        return list;
    }

}
