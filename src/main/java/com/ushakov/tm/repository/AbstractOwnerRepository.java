package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.IOwnerRepository;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity>
        extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    @Override
    public E add(final String userId, final E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public void clear(final String userId) {
        list.removeAll(list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList()));
    }

    @Override
    public List<E> findAll(final String userId) {
        return list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(final String userId, Comparator<E> comparator) {
        return list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public E findOneById(final String userId, final String id) {
        return findAll(userId).stream()
                .filter(e -> e.getId().equals(id))
                .findFirst().orElse(null);
    }

    @Override
    public E removeOneById(final String userId, final String id) {
        final E entity =  findOneById(userId, id);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

    @Override
    public E findOneByIndex(final String userId, final Integer index) {
            final E entity = list.get(index);
            if (userId.equals(entity.getUserId())) return entity;
            return null;
    }

    @Override
    public E removeOneByIndex(final String userId, final Integer index) {
        final E entity = findOneByIndex(userId, index);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

}