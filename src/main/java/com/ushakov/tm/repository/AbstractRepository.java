package com.ushakov.tm.repository;

import com.ushakov.tm.api.IRepository;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public E add(final E entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public E findOneById(final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void remove(final E entity) {
        list.remove(entity);
    }

    @Override
    public E removeOneById(final String id) {
        final E entity = findOneById(id);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

}
