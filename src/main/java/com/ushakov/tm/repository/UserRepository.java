package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.IUserRepository;
import com.ushakov.tm.exception.entity.UserNotFoundException;
import com.ushakov.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findOneByEmail(final String email) {
        return list.stream()
                .filter(u -> u.getEmail().equals(email))
                .findFirst().orElse(null);
    }

    @Override
    public User findOneByLogin(final String login) {
        return list.stream()
                .filter(u -> u.getLogin().equals(login))
                .findFirst().orElse(null);
    }

    @Override
    public User removeOneByLogin(final String login) {
        final User user = findOneByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        remove(user);
        return user;
    }

}
