package com.ushakov.tm.command;

import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;

import java.util.Optional;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Start Date: " + task.getDateStart());
        System.out.println("Finish Date: " + task.getDateEnd());
        System.out.println("Created: " + task.getCreated());
    }

}
