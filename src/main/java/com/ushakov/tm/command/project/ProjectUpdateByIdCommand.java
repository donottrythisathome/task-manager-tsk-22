package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update project by id.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findOneById(userId, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("ENTER NAME");
        final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String projectDescription = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().updateProjectById(userId, projectId, projectName, projectDescription) == null) {
            throw new ProjectNotFoundException();
        }
    }

    @Override
    public String name() {
        return "update-project-by-id";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
