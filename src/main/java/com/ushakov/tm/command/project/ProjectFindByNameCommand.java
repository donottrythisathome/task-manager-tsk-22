package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectFindByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Find project by name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER PROJECT NAME");
        final String projectName = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findOneByName(userId, projectName);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println(project);
    }

    @Override
    public String name() {
        return "find-project-by-name";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
