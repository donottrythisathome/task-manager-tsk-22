package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectDeleteByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete project with child tasks.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectTaskService().deleteProjectById(userId, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public String name() {
        return "delete-project-with-tasks";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
