package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Complete task by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        final Task task = serviceLocator.getTaskService().completeTaskByIndex(userId, taskIndex - 1);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public String name() {
        return "complete-task-by-index";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
