package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskFindByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Find task by id.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findOneById(userId, taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println(task);
    }

    @Override
    public String name() {
        return "find-task-by-id";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
