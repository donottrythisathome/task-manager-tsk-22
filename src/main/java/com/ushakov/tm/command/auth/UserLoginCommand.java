package com.ushakov.tm.command.auth;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "New login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

    @Override
    public String name() {
        return "login";
    }

}
