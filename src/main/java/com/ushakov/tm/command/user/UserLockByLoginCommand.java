package com.ushakov.tm.command.user;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.util.TerminalUtil;

public class UserLockByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Lock user by login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockByLogin(login);
        System.out.println("OK");
    }

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
