package com.ushakov.tm.service;

import com.ushakov.tm.Application;
import com.ushakov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

public class LoggerService implements ILoggerService {

    private static final String COMMANDS = "COMMANDS";
    private static final String COMMANDS_FILE = "./commands.txt";

    private static final String ERRORS = "ERRORS";
    private static final String ERRORS_FILE = "./errors.txt";

    private static final String MESSAGES = "MESSAGES";
    private static final String MESSAGES_FILE = "./messages.txt";

    private final LogManager manager = LogManager.getLogManager();
    private final Logger root = Logger.getLogger("");
    private final Logger commands = Logger.getLogger(COMMANDS);
    private final Logger messages = Logger.getLogger(MESSAGES);
    private final Logger errors = Logger.getLogger(ERRORS);

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(messages, MESSAGES_FILE, true);
        registry(errors, ERRORS_FILE, true);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        messages.fine(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    private void init() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream("/logger.properties"));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry(final Logger logger, final String filename, final boolean consoleEnable) {
        try {
            if (consoleEnable) logger.addHandler(getConsoleHandler());
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(filename));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

}
