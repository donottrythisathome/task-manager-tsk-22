package com.ushakov.tm.exception.entity;

import com.ushakov.tm.exception.AbstractException;

public class UserAlreadyExistsException extends AbstractException {

    public UserAlreadyExistsException(final String value) {
        super("Error! User with same " + value + " already exists!");
    }

}
