package com.ushakov.tm.exception.entity;

import com.ushakov.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project was not found!");
    }

}
